﻿using NUnit.Framework;
using StateMachine.Loaders;
using StateMachine.Loaders.Json;

namespace StateMachine.Tests
{
    [TestFixture]
    public class JsonLoaderTests
    {
        [Test]
        public void TestCanLoadSimpleOneStateMachine()
        {
            var machineJson = "[ { id: 'entry-rule', caption: 'start', type: 'simple', isStart: true } ]";

            var loader = new JsonStateMachineFileLoader();

            var machine = loader.Load(machineJson);

            Assert.IsNotNull(machine);
            Assert.AreEqual(1, machine.States.Count);

            Assert.AreEqual("entry-rule", machine.States[0].Id);
            Assert.AreEqual("start", machine.States[0].Caption);
            Assert.IsTrue(machine.States[0].IsEntry);
        }

        [Test]
        public void TestCanLoadSimpleTransitionMachine()
        {
            var machineJson = "[{ id: 'entry-rule', caption: 'start', type: 'simple', isStart: true, transitionsTo: 'ending-state' }, { id: 'ending-state', caption: 'The End', type: 'simple', isEnd: true } ]";

            var loader = new JsonStateMachineFileLoader();

            var machine = loader.Load(machineJson);
            var input = StateMachineUtils.CreateSampleRunObject();
            var result = machine.Process(input);

            Assert.IsNotNull(result);
            Assert.AreEqual("ending-state", result.Id);
        }

        [Test]
        public void TestCanLoadConditionalTransitionMachine()
        {
            #region Machine JSON
            var machineJson = @"[ {
		                            id: 'entry-state',
		                            caption: 'start here',
		                            type: 'simple',
		                            isStart: true,
		                            transitionsTo: 'condition-state'
	                            },
	                            {
		                            id: 'condition-success',
		                            caption: 'Condition was true, total prices was less than £100',
		                            type: 'simple',
		                            isEnd: true
	                            },
	                            {
		                            id: 'condition-fail',
		                            caption: 'Condition was not true, total prices was more than £100',
		                            type: 'simple',
		                            isEnd: true
	                            },
	                            {
		                            id: 'condition-state',
		                            caption: '< £100?',
		                            type: 'conditional',
		                            condition: {
			                            type: 'simple',
			                            property: 'totalPrice',
			                            operation: 'lessThan',
			                            operand: '100'
		                            },
		                            trueTransition: 'condition-success',
		                            falseTransition: 'condition-fail'
	                            }
                            ]";
            #endregion

            var loader = new JsonStateMachineFileLoader();

            var machine = loader.Load(machineJson);
            var input = StateMachineUtils.CreateSampleRunObject();
            var result = machine.Process(input);

            Assert.IsNotNull(result);
            Assert.AreEqual("condition-success", result.Id);
        }
    }
}

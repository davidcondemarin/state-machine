﻿using NUnit.Framework;
using StateMachine.States;

namespace StateMachine.Tests
{
    [TestFixture]
    public class StateMachineSimpleStateTests
    {
        [Test]
        public void TestStateMachineWithOneNode()
        {
            var stateMachine = new StateMachine();
            var entryState = new SimpleTransitionState("entry", true, false);
            var argument = StateMachineUtils.CreateSampleRunObject();

            stateMachine.States.Add(entryState);

            var endState = stateMachine.Process(argument);

            Assert.IsNotNull(endState);
            Assert.AreEqual("entry", endState.Id);
        }

        [Test]
        public void TestStateMachineWithSimpleTransition()
        {
            var stateMachine = new StateMachine();
            var entryState = new SimpleTransitionState("entry", true, false) {TransitionToStateId = "second-state"};
            var secondState = new SimpleTransitionState("second-state", false, false);

            var argument = StateMachineUtils.CreateSampleRunObject();

            stateMachine.States.Add(entryState);
            stateMachine.States.Add(secondState);

            var endState = stateMachine.Process(argument);

            Assert.IsNotNull(endState);
            Assert.AreEqual("second-state", endState.Id);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using StateMachine.States;

namespace StateMachine.Tests
{
    [TestFixture]
    public class StateMachineInitialisationTests
    {
        [Test]
        public void TestStateMachineStartsEmpty()
        {
            var stateMachine = new StateMachine();
            
            Assert.IsNotNull(stateMachine.States);
            Assert.AreEqual(0, stateMachine.States.Count);
        }

        [Test]
        public void TestRunEmptyStateMachineThrowsException()
        {
            var stateMachine = new StateMachine();
            var argument = StateMachineUtils.CreateSampleRunObject();

            Assert.Throws<InvalidOperationException>(() => stateMachine.Process(argument));
        }

        [Test]
        public void TestRunStateMachineWithoutStartThrowsException()
        {
            var stateMachine = new StateMachine();
            var argument = StateMachineUtils.CreateSampleRunObject();
            stateMachine.States.Add(new ConditionalState("condition-1", false, false));

            Assert.Throws<InvalidOperationException>(() => stateMachine.Process(argument));
        }

        [Test]
        public void TestRunStateMachineWithMultipleEntriesThrowsException()
        {
            var stateMachine = new StateMachine();
            var argument = StateMachineUtils.CreateSampleRunObject();

            stateMachine.States.Add(new ConditionalState("condition-1", true, false));
            stateMachine.States.Add(new ConditionalState("condition-2", true, false));

            Assert.Throws<InvalidOperationException>(() => stateMachine.Process(argument));
        }

        [Test]
        public void TestRunStateMachineWithoutArgumentsThrowsException()
        {
            var stateMachine = new StateMachine();
            var argument = new Dictionary<string, string>();
            stateMachine.States.Add(new ConditionalState("condition-1", true, false));

            Assert.Throws<InvalidOperationException>(() => stateMachine.Process(argument));
        }
    }
}

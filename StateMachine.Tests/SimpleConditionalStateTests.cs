﻿using NUnit.Framework;
using StateMachine.Conditions;
using StateMachine.States;

namespace StateMachine.Tests
{
    [TestFixture]
    public class SimpleConditionalStateTests 
    {
        private StateMachine _machine;

        [SetUp]
        public void Setup()
        {
            _machine = new StateMachine();

            var conditionTrueState = new SimpleTransitionState("condition-true", false, false);
            var conditionFalseState = new SimpleTransitionState("condition-false", false, false);

            _machine.States.Add(conditionTrueState);
            _machine.States.Add(conditionFalseState);
        }

        [Test]
        [TestCase("bad string value", "string-prop", "condition-false")]
        [TestCase("string value", "string-prop", "condition-true")]
        public void TestEqualsCondition(string operand, string property, string result)
        {
            var conditionalState = new ConditionalState("state", true, false)
            {
                Condition = new EqualsCondition { Operand = operand, Property = property },
                TrueStateTransitionId = "condition-true",
                FalseStateTransitionId = "condition-false"
            };

            _machine.States.Add(conditionalState);

            var entry = StateMachineUtils.CreateSampleRunObject();
            var finalState = _machine.Process(entry);

            Assert.AreEqual(finalState.Id, result);
        }

        [Test]
        [TestCase("16", "decimal-prop", "condition-true")]
        [TestCase("1", "numeric-prop", "condition-false")]
        [TestCase("15.9899", "decimal-prop", "condition-false")]
        [TestCase("15.9901", "decimal-prop", "condition-true")]
        public void TestLessThanCondition(string operand, string property, string result)
        {
            var conditionalState = new ConditionalState("state", true, false)
            {
                Condition = new LessThanCondition { Operand = operand, Property = property },
                TrueStateTransitionId = "condition-true",
                FalseStateTransitionId = "condition-false"
            };

            _machine.States.Add(conditionalState);

            var entry = StateMachineUtils.CreateSampleRunObject();
            var finalState = _machine.Process(entry);

            Assert.AreEqual(finalState.Id, result);
        }

        [Test]
        [TestCase("16", "decimal-prop", "condition-true")]
        [TestCase("15", "decimal-prop", "condition-false")]
        [TestCase("101", "decimal-prop", "condition-true")]
        [TestCase("15.9901", "decimal-prop", "condition-true")]
        public void TestGreaterThanCondition(string operand, string property, string result)
        {
            var conditionalState = new ConditionalState("state", true, false)
            {
                Condition = new GreaterThanCondition { Operand = operand, Property = property },
                TrueStateTransitionId = "condition-true",
                FalseStateTransitionId = "condition-false"
            };

            _machine.States.Add(conditionalState);

            var entry = StateMachineUtils.CreateSampleRunObject();
            var finalState = _machine.Process(entry);

            Assert.AreEqual(finalState.Id, result);
        }
    }
}

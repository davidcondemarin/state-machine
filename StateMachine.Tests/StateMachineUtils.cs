﻿using System.Collections.Generic;

namespace StateMachine.Tests
{
    public static class StateMachineUtils
    {
        public static Dictionary<string, string> CreateSampleRunObject()
        {
            return new Dictionary<string, string> {
                { "string-prop", "string value" },
                { "numeric-prop", "15" },
                { "decimal-prop", "15.99" },
                { "totalPrice", "15.99" }
            };
        }

    }
}
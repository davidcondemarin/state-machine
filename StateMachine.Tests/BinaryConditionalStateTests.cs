﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using StateMachine.Conditions;
using StateMachine.States;

namespace StateMachine.Tests
{
    [TestFixture]
    public class BinaryConditionalStateTests
    {
        private StateMachine _machine;

        [SetUp]
        public void Setup()
        {
            _machine = new StateMachine();

            var conditionTrueState = new SimpleTransitionState("condition-true", false, false);
            var conditionFalseState = new SimpleTransitionState("condition-false", false, false);

            _machine.States.Add(conditionTrueState);
            _machine.States.Add(conditionFalseState);
        }

        //p q p ^ q
        //1 1   1 
        //1 0   0
        //0 1   0
        //0 0   0
        [Test]
        [TestCase("15", "15", "condition-true")] 
        [TestCase("15", "16", "condition-false")] 
        [TestCase("16", "15", "condition-false")] 
        [TestCase("17", "16", "condition-false")] 
        public void TestAndOperator(string p, string q, string expected)
        {
            var leftCondition = new EqualsCondition { Operand = p, Property = "numeric-prop" };
            var rightCondition = new EqualsCondition { Operand = q, Property = "numeric-prop" };

            var conditionalState = new ConditionalState("state", true, false) {
                Condition = new AndCondition { Left = leftCondition, Right = rightCondition },
                TrueStateTransitionId = "condition-true",
                FalseStateTransitionId = "condition-false"
            };

            _machine.States.Add(conditionalState);

            var entry = StateMachineUtils.CreateSampleRunObject();
            var finalState = _machine.Process(entry);

            Assert.AreEqual(finalState.Id, expected);
        }

        //p q p v q
        //1 1   1 
        //1 0   1
        //0 1   1
        //0 0   0
        [Test]
        [TestCase("15", "15", "condition-true")]
        [TestCase("15", "16", "condition-true")]
        [TestCase("16", "15", "condition-true")]
        [TestCase("17", "16", "condition-false")]
        public void TestOrOperator(string p, string q, string expected)
        {
            var leftCondition = new EqualsCondition { Operand = p, Property = "numeric-prop" };
            var rightCondition = new EqualsCondition { Operand = q, Property = "numeric-prop" };

            var conditionalState = new ConditionalState("state", true, false)
            {
                Condition = new OrCondition { Left = leftCondition, Right = rightCondition },
                TrueStateTransitionId = "condition-true",
                FalseStateTransitionId = "condition-false"
            };

            _machine.States.Add(conditionalState);

            var entry = StateMachineUtils.CreateSampleRunObject();
            var finalState = _machine.Process(entry);

            Assert.AreEqual(finalState.Id, expected);
        }
    }
}

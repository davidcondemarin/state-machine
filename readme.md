﻿# State machine library for CSharp

This is a quick implementation of state machines for C#. Far from complete yet and not tested on the wild but a good starting point to delve into the world of state machines

## Sample usage

The following example creates a state machine with a conditional start that checks for a property of numeric value and if it's over 100 it goes to true otherwise it goes to false

	var stateMachine = new StateMachine();

	var conditionTrueState = new SimpleTransitionState("condition-true", false, false);
	var conditionFalseState = new SimpleTransitionState("condition-false", false, false);

	var conditionalState = new ConditionalState("state", true, false) {
		Caption = "Is the order value more than £100",
		Condition = new GreaterThanCondition { Operand = "order.value", Property = "100" },
		TrueStateTransitionId = "condition-true",
		FalseStateTransitionId = "condition-false"
	};

	stateMachine.States.Add(conditionTrueState);
	stateMachine.States.Add(conditionFalseState);
	stateMachine.States.Add(conditionalState);

	var inputForExpensiveOrder = new Dictionary<string, string>{ {"order.value", "150"} };
	var inputForCheapOrder = new Dictionary<string, string>{ {"order.value", "90"} };

	var cheapResult = stateMachine.Process(inputForCheapOrder); //cheapResult => condition-true
	var expensiveResult = stateMachine.Process(inputForExpensiveOrder); //cheapResult => condition-true
﻿namespace StateMachine.Conditions
{
    public abstract class BinaryCondition : Condition
    {
        public Condition Left { get; set; }

        public Condition Right { get; set; }
    }
}
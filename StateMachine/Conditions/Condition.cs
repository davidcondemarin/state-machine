﻿using System.Collections.Generic;

namespace StateMachine.Conditions
{
    public abstract class Condition
    {
        public abstract bool Operate(Dictionary<string, string> context);
    }
}
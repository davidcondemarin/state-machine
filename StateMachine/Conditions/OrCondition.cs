﻿using System.Collections.Generic;

namespace StateMachine.Conditions
{
    public class OrCondition : BinaryCondition
    {
        public override bool Operate(Dictionary<string, string> context)
        {
            return Left.Operate(context) || Right.Operate(context);
        }
    }
}
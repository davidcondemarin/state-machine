using System;
using System.Collections.Generic;

namespace StateMachine.Conditions
{
    public class EqualsCondition : SimpleCondition
    {
        public override bool Operate(Dictionary<string, string> context)
        {
            var propValue = AssesedPropertyValue(context);

            if (decimal.TryParse(propValue, out var decimalProp) && decimal.TryParse(Operand, out var decimalOperand))
            {
                return decimalOperand == decimalProp;
            }

            return string.Compare(Operand, AssesedPropertyValue(context), StringComparison.Ordinal) == 0;
        }
    }
}
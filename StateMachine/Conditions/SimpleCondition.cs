﻿using System.Collections.Generic;

namespace StateMachine.Conditions
{
    public abstract class SimpleCondition : Condition
    {
        public string Property { get; set; }

        public string Operand { get; set; }

        protected string AssesedPropertyValue(Dictionary<string, string> context) => context.ContainsKey(Property) ? context[Property] : string.Empty;
    }
}
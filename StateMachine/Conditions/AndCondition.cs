﻿using System.Collections.Generic;

namespace StateMachine.Conditions
{
    public class AndCondition : BinaryCondition
    {
        public override bool Operate(Dictionary<string, string> context)
        {
            return Left.Operate(context) && Right.Operate(context);
        }
    }
}
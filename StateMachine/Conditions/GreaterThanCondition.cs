using System;
using System.Collections.Generic;

namespace StateMachine.Conditions
{
    public class GreaterThanCondition : SimpleCondition
    {
        public override bool Operate(Dictionary<string, string> context)
        {
            var propValue = AssesedPropertyValue(context);

            if (decimal.TryParse(propValue, out var decimalProp) && decimal.TryParse(Operand, out var decimalOperand))
            {
                return decimalOperand > decimalProp;
            }

            return string.Compare(Operand, propValue, StringComparison.Ordinal) > 0;
        }
    }
}
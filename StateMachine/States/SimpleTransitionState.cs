﻿using System.Collections.Generic;

namespace StateMachine.States
{
    public class SimpleTransitionState : State
    {
        public SimpleTransitionState(string id, bool isEntry, bool isFinal)
        {
            Id = id;
            IsEntry = isEntry;
            IsFinal = isFinal;
        }

        public string TransitionToStateId { get; set; }

        public override bool CanTransition()
        {
            return !string.IsNullOrEmpty(TransitionToStateId);
        }

        public override string NextState(Dictionary<string, string> input)
        {
            return TransitionToStateId;
        }
    }
}

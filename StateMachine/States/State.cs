﻿using System.Collections.Generic;

namespace StateMachine.States
{
    public abstract class State
    {
        public string Id { get; set; }

        public string Caption { get; set; }

        public bool IsEntry { get; protected set; }

        public bool IsFinal { get; protected set; }

        public abstract bool CanTransition();

        public abstract string NextState(Dictionary<string, string> input);
    }
}

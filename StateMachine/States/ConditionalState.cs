﻿using System;
using System.Collections.Generic;
using StateMachine.Conditions;

namespace StateMachine.States
{
    public class ConditionalState : State
    {
        public ConditionalState(string id, bool isEntry, bool isFinal)
        {
            Id = id;
            IsEntry = isEntry;
            IsFinal = isFinal;
        }

        public Condition Condition { get; set; }

        public string TrueStateTransitionId { get; set; }

        public string FalseStateTransitionId { get; set; }

        public override bool CanTransition()
        {
            var hasTransitions = !string.IsNullOrEmpty(TrueStateTransitionId) &&
                                 !string.IsNullOrEmpty(FalseStateTransitionId);

            return Condition != null && hasTransitions;
        }

        public override string NextState(Dictionary<string, string> input)
        {
            var result = Condition.Operate(input);
            return result ? TrueStateTransitionId : FalseStateTransitionId;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using StateMachine.States;

namespace StateMachine
{
    public class StateMachine
    {
        public List<State> States { get; }

        public StateMachine()
        {
            States = new List<State>();
        }

        public State Process(Dictionary<string, string> input)
        {
            CheckOperation(input.Count == 0, "Cannot run a state machine without an input");
            CheckOperation(States.Count == 0, "Cannot run a state machine with no states");
            CheckOperation(States.Count(s => s.IsEntry) > 1, "State machines cant have more than one entry point");

            var start = States.SingleOrDefault(s => s.IsEntry);

            CheckOperation(start == null, "Cannot run a state machine without an entry state");

            var currentState = start;
            var canTransition = currentState.CanTransition();

            while (canTransition)
            {
                var nextState = currentState.NextState(input);
                var nextStateNode = States.FirstOrDefault(s => s.Id == nextState);

                if (nextStateNode == null)
                {
                    canTransition = false;
                }
                else
                {
                    currentState = nextStateNode;
                    canTransition = currentState.CanTransition();
                }
            }

            return currentState;
        }

        private void CheckOperation(bool condition, string exceptionMessage)
        {
            if (condition)
                throw new InvalidOperationException(exceptionMessage);
        }
    }
}

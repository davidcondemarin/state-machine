﻿using StateMachine.Conditions;

namespace StateMachine.Loaders.Json
{
    internal class ConditionStateFactory
    {
        public Condition Create(RawJsonCondition condition)
        {
            switch (condition.Type)
            {
                default:
                    return CreateSimpleCondition(condition);
            }
        }

        public Condition CreateSimpleCondition(RawJsonCondition condition)
        {
            SimpleCondition createdCondition;

            switch (condition.Operation)
            {
                case "lessThan":
                    createdCondition = new LessThanCondition();
                    break;
                case "greaterThan":
                    createdCondition = new GreaterThanCondition();
                    break;
                default:
                    createdCondition = new EqualsCondition();
                    break;
            }

            createdCondition.Operand = condition.Operand;
            createdCondition.Property = condition.Property;

            return createdCondition;
        }
    }
}
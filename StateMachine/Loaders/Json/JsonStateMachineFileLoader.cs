﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace StateMachine.Loaders.Json
{
    public class JsonStateMachineFileLoader
    {
        private JsonStateFactory _jsonStateFactory = new JsonStateFactory();

        public StateMachine Load(string fileContents)
        {
            var stateMachine = new StateMachine();

            var rawStates = JsonConvert.DeserializeObject<List<RawJsonState>>(fileContents);

            foreach (var state in rawStates)
            {
                var machineState = _jsonStateFactory.Create(state);

                stateMachine.States.Add(machineState);
            }

            return stateMachine;
        }
    }
}

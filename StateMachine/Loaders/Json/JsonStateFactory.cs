﻿using StateMachine.States;

namespace StateMachine.Loaders.Json
{
    internal class JsonStateFactory
    {
        private readonly ConditionStateFactory _conditionStateFactory = new ConditionStateFactory();

        public State Create(RawJsonState jsonState)
        {
            switch (jsonState.Type)
            {
                case "conditional":
                    return ConditionalStateFromRawJson(jsonState);

                default:
                    return SimpleTransitionFromRawJson(jsonState);
            }
        }

        private State ConditionalStateFromRawJson(RawJsonState state)
        {
            var conditional = new ConditionalState(state.Id, state.IsStart, state.IsEnd)
            {
                TrueStateTransitionId = state.TrueTransition,
                FalseStateTransitionId = state.FalseTransition,
                Condition = _conditionStateFactory.CreateSimpleCondition(state.Condition)
            };

            return conditional;
        }

        private State SimpleTransitionFromRawJson(RawJsonState state)
        {
            return new SimpleTransitionState(state.Id, state.IsStart, state.IsEnd)
            {
                Caption = state.Caption,
                TransitionToStateId = state.TransitionsTo
            };
        }
    }
}

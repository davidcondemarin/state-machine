namespace StateMachine.Loaders.Json
{
    internal class RawJsonState
    {
        public string Id { get; set; }

        public string Caption { get; set; }

        public string Type { get; set; }

        public string TrueTransition { get; set; }

        public string FalseTransition { get; set; }

        public string TransitionsTo { get; set; }

        public bool IsStart { get; set; }

        public bool IsEnd { get; set; }

        public RawJsonCondition Condition { get; set; }
    }
}
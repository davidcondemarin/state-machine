namespace StateMachine.Loaders.Json
{
    internal class RawJsonCondition
    {
        public string Type { get; set; }

        public string Property { get; set; }

        public string Operation { get; set; }

        public string Operand { get; set; }

        public RawJsonCondition Left { get; set; }

        public RawJsonCondition Right { get; set; }
    }
}